from django.db.models.query import QuerySet
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework import filters, generics, serializers, status, viewsets
from rest_framework.response import Response

from rest_framework.views import APIView
from bussiness.api.serializers import BussinessSerializer, ProductTypeSerializer
from user.api.serializers import UserSerializer
from bussiness.models import Bussiness, ProductType

from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from rest_framework.decorators import api_view

from django.conf import settings
from django.core.mail import send_mail

from fcm_django.models import FCMDevice
from rest_framework.authtoken.models import Token
from firebase_admin.messaging import Message, Notification


class BussinessList(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Bussiness.objects.all()
    serializer_class = BussinessSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['nameBussiness', 'bussiness_user', 'active']

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            # If user is admin, user can get all bussiness
            return Bussiness.objects.all()
        else:
            # If user is not admin, user just gets user's bussiness
            return Bussiness.objects.filter(bussiness_user=user)

    # def post(self, request):
    #     serializer = BussinessSerializer(data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()

    #         return Response(serializer.data, status=status.HTTP_200_OK)
    #     else:
    #         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BussinessCreate(generics.CreateAPIView):
    serializer_class = BussinessSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        # Override the add current user
        bussiness_user = self.request.user
        bussiness_queryset = Bussiness.objects.filter(
            bussiness_user=bussiness_user)
        serializer.save(bussiness_user=bussiness_user)

        data = serializer.data
        bussiness_id = Bussiness.objects.get(
            id=data['id'])  # get name bussiness
        bussiness_user_id = self.request.user.id  # get id current user

        # send mail
        subject = f'Account {self.request.user.email} registry new bussiness'
        message = f'There is a new bussiness from {self.request.user.email}. Please click http://127.0.0.1:8000/bussiness/list/?nameBussiness={bussiness_id}&bussiness_user={bussiness_user_id}&active=false to go to bussiness management'
        email_from = self.request.user.email
        recipient_list = [settings.EMAIL_HOST_USER, ]
        send_mail(subject, message, email_from, recipient_list)

        # fcm Notification
        # fcm will a token of device, Use "nameBussiness" to test
        fcm = data['nameBussiness']
        device = FCMDevice()
        device.user = bussiness_user
        device.registration_id = fcm
        device.type = "web"
        device.name = "Can be anything"
        device.save()
        # title = f'You created successfully a new bussiness {fcm}'
        # body = "Please wait for admin approves"
        devices = FCMDevice.objects.get(user=bussiness_user)
        result = devices.send_message(
            Message(
                notification=Notification(
                    title="title", body="text", image="url")
            )
        )
        print(result)


class BussinessDetail(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = BussinessSerializer

    def get(self, request, pk):
        # get current user
        user = self.request.user
        # try catch exception to test id exist or not
        try:
            if user.is_staff:
                bussiness = Bussiness.objects.get(pk=pk)
            else:
                bussiness = Bussiness.objects.get(pk=pk, bussiness_user=user)
        except Bussiness.DoesNotExist:
            return Response({'error': 'Bussiness is not found'}, status=status.HTTP_404_NOT_FOUND)
        serializer = BussinessSerializer(bussiness)

        return Response(serializer.data)

    def put(self, request, pk):
        user = self.request.user
        if user.is_staff:
            bussiness = Bussiness.objects.get(pk=pk)
        else:
            bussiness = Bussiness.objects.get(pk=pk, bussiness_user=user)
        serializer = BussinessSerializer(bussiness, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        user = self.request.user
        if user.is_staff:
            bussiness = Bussiness.objects.get(pk=pk)
        else:
            bussiness = Bussiness.objects.get(pk=pk, bussiness_user=user)
        bussiness.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)


class ProductTypeList(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = ProductType.objects.all()
    serializer_class = ProductTypeSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name', 'active']

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            # If user is admin, user can get all bussiness
            return ProductType.objects.all()
        else:
            # If user is not admin, user just gets user's bussiness
            return ProductType.objects.filter(bussiness_user=user)

    # def post(self, request):
    #     serializer = ProductTypeSerializer(data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()

    #         return Response(serializer.data, status=status.HTTP_200_OK)
    #     else:
    #         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProductTypeCreate(generics.CreateAPIView):
    serializer_class = ProductTypeSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        # Override the add current user
        bussiness_user = self.request.user
        bussiness_queryset = ProductType.objects.filter(
            bussiness_user=bussiness_user)
        serializer.save(bussiness_user=bussiness_user)


class ProductTypeDetail(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = ProductTypeSerializer

    def get(self, request, pk):
        # get current user
        user = self.request.user
        # try catch exception to test id exist or not
        try:
            if user.is_staff:
                product_type = ProductType.objects.get(pk=pk)
            else:
                product_type = ProductType.objects.get(
                    pk=pk, bussiness_user=user)
        except ProductType.DoesNotExist:
            return Response({'error': 'Product Type is not found'}, status=status.HTTP_404_NOT_FOUND)
        serializer = ProductTypeSerializer(product_type)

        return Response(serializer.data)

    def put(self, request, pk):
        user = self.request.user
        if user.is_staff:
            product_type = ProductType.objects.get(pk=pk)
        else:
            product_type = ProductType.objects.get(pk=pk, bussiness_user=user)
        serializer = ProductTypeSerializer(product_type, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        user = self.request.user
        if user.is_staff:
            product_type = ProductType.objects.get(pk=pk)
        else:
            product_type = ProductType.objects.get(pk=pk, bussiness_user=user)
        product_type.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)
