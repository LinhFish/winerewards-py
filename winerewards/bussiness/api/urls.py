from django.urls import path, include
from bussiness.api import views
from rest_framework import routers
from fcm_django.api.rest_framework import FCMDeviceAuthorizedViewSet

router = routers.DefaultRouter()
router.register('devices', FCMDeviceAuthorizedViewSet)

urlpatterns = [

    path('list/', views.BussinessList.as_view(),
         name='bussiness-list'),
    path('<int:pk>/', views.BussinessDetail.as_view(),
         name='bussiness-details'),
    path('create/', views.BussinessCreate.as_view(),
         name='bussiness-create'),
    path('', include(router.urls)),

    path('list/type_product/', views.ProductTypeList.as_view(),
         name='type-product-list'),
    path('list/type_product/<int:pk>/', views.ProductTypeDetail.as_view(),
         name='type-product-details'),
    path('create/type_product/', views.ProductTypeCreate.as_view(),
         name='type_product-create'),
    path('', include(router.urls)),
]
