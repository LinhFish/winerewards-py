from rest_framework import serializers
from bussiness.models import Bussiness , ProductType
from user.api.serializers import UserSerializer
# from django.conf import settings


# Validate function length of name bussiness
def name_length(value):
    if len(value) < 2:
        raise serializers.ValidationError("Name is too short")
    if len(value) > 50:
        raise serializers.ValidationError("Name is too long")


class BussinessSerializer(serializers.Serializer):

    class Meta:
        model = Bussiness
        fields = "__all__"
    # ForeignKey User
    bussiness_user = serializers.StringRelatedField(read_only=True)
    # bussiness_user = UserSerializer(required=True)
    id = serializers.IntegerField(read_only=True)
    nameBussiness = serializers.CharField(validators=[name_length])
    descriptionBussiness = serializers.CharField()
    pictureBussiness = serializers.CharField()
    typeProduct = serializers.CharField()
    addressBussiness = serializers.CharField()
    openTime = serializers.TimeField()
    closeTime = serializers.TimeField()
    active = serializers.BooleanField()
    create = serializers.DateTimeField()
    update = serializers.DateTimeField()

    def create(self, validated_data):
        return Bussiness.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.nameBussiness = validated_data.get(
            'nameBussiness', instance.nameBussiness)
        instance.descriptionBussiness = validated_data.get(
            'descriptionBussiness', instance.descriptionBussiness)
        instance.pictureBussiness = validated_data.get(
            'pictureBussiness', instance.pictureBussiness)
        instance.typeProduct = validated_data.get(
            'typeProduct', instance.typeProduct)
        instance.addressBussiness = validated_data.get(
            'addressBussiness', instance.addressBussiness)
        instance.openTime = validated_data.get('openTime', instance.openTime)
        instance.closeTime = validated_data.get(
            'closeTime', instance.descriptionBussiness)
        instance.active = validated_data.get('active', instance.active)
        instance.save()
        return instance


# Validate function length of name type product
def name_length(value):
    if len(value) < 2:
        raise serializers.ValidationError("Name is too short")
    if len(value) > 20:
        raise serializers.ValidationError("Name is too long")

class ProductTypeSerializer(serializers.Serializer):
    
    class Meta:
        model = ProductType
        fields = "__all__"
    bussiness_user = serializers.StringRelatedField(read_only=True)
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(validators=[name_length])
    description = serializers.CharField()
    active = serializers.BooleanField()
    create = serializers.DateTimeField()
    update = serializers.DateTimeField()

    def create(self, validated_data):
        return ProductType.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get(
            'name', instance.name)
        instance.description = validated_data.get(
            'description', instance.description)
        instance.active = validated_data.get('active', instance.active)
        instance.save()
        return instance
