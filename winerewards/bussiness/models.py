from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Bussiness(models.Model):
    # Relate with User
    bussiness_user = models.ForeignKey(
        User, on_delete=models.CASCADE)

    nameBussiness = models.CharField(max_length=50)
    descriptionBussiness = models.TextField()
    pictureBussiness = models.CharField(max_length=255)
    typeProduct = models.CharField(max_length=50)
    addressBussiness = models.TextField()
    openTime = models.TimeField()
    closeTime = models.TimeField()
    active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nameBussiness

class ProductType(models.Model):
    bussiness_user = models.ForeignKey(
        User, on_delete=models.CASCADE)

    name = models.CharField(max_length=50)
    description = models.TextField()
    active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)

    def __str__(self):
            return self.name



