from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('account/', include('user.api.urls')),
    path('bussiness/', include('bussiness.api.urls')),

]
