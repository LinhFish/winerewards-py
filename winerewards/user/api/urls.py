from django.urls import path
from rest_framework import views
from rest_framework.authtoken.views import obtain_auth_token
from user.api import views
from rest_framework_simplejwt.views import TokenObtainPairView,TokenRefreshView

urlpatterns = [
    path('login/', obtain_auth_token, name='login'),
    path('register/', views.registration_view, name='register'),
    path('logout/', views.logout_view, name='logout'),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('change-password/', views.ChangePasswordView.as_view(), name='change_password'),
    path('profile/', views.CurrentUserView.as_view(), name='profile'),
    path('list/', views.UsersListView.as_view(), name='list_users'),
]
